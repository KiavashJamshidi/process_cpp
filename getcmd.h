#ifndef GETCMD_H
#define GETCMD_H
#include <iostream>
#include <string>
#include <vector>
#include <dirent.h>
#include <filesystem>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>

using namespace std;

void getCmd(string filename,vector<string> &cmds);
void splitCmds(vector<string> cmds,vector<string> &maxmins,vector<string> &productIDs,
 vector<string> &startDates,vector<string> &endDates);
bool fileIsCsv(char name[256],int i) ;
void findCsvFiles(char name[256]) ;
bool dirDoesntExist(DIR* parentDir);
bool processIsParent(int pid);
bool dateIsAfterStart(string date,string startDate);
bool dateIsBeforeEnd(string date,string endDate);
bool processIfThisIsCsvChar(char name[256],string path,vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int eachCmd,fstream &resultFile);
bool processIsChild(int pid);
void parentWriteRead(vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int fd[2]);
void childReadWrite(vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int fd[2],char (&result)[256],char (&resultone)[256],
  char (&resulttwo)[256],char (&resultthree)[256]);
void readDir(string path,vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int whichLevel,int eachCmd,fstream &file);
void showFinalResult(fstream& file,vector<string> maxmins,int eachCmd);

#endif