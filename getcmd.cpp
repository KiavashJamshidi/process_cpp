#include "getcmd.h"
#include <limits>
#include <sys/stat.h>

void getCmd(string filename,vector<string> &cmds) {
    fstream file;
    file.open(filename);
    string line;
    while(getline(file,line,'\n'))
        cmds.push_back(line);
}

void splitCmds(vector<string> cmds,vector<string> &maxmins,vector<string> &productIDs,
 vector<string> &startDates,vector<string> &endDates) {
    string tmp = "";
    int numOfSpaces = 0;
    for (int i=0; i<cmds.size();i++) {
        tmp = "";
        numOfSpaces = 0;
        for (int j=0;j<cmds[i].size();j++) {
            if (cmds[i][j] == ' ' || j + 1 == cmds[i].size()) {
                numOfSpaces++;
                if (numOfSpaces == 1)
                    maxmins.push_back(tmp);
                else if (numOfSpaces == 2)
                    productIDs.push_back(tmp);
                else if (numOfSpaces == 3)
                    startDates.push_back(tmp);
                else if (numOfSpaces == 4 && j + 1 != cmds[i].size()){
                    endDates.push_back(tmp);
                    break;
                }
                else if (numOfSpaces == 4 && j + 1 == cmds[i].size())
                    endDates.push_back(tmp + cmds[i][j]);
                tmp = "";
                continue;
            }
            tmp += cmds[i][j];
        }
    }
}

bool dirDoesntExist(DIR* parentDir){
    return parentDir == NULL;
}


bool fileIsCsvChar(char name[256],int i) {
    return name[i] == '.' && name[i + 1] == 'c' && name[i + 2] == 's' && name[i + 3] == 'v';
}

bool processIsParent(int pid){
    return pid > 0;
}

bool dateIsAfterStart(string date,string startDate){
    int yearDate,yearStart,monthDate,monthStart,day,dayStart;
    string tmp = "";
    int num = 0;
    for (int i=0;i<date.size();i++){
        if (date[i] == '/') {
            num++;
            if (num == 1)
                yearDate = stoi(tmp);
            else if (num == 2)
                monthDate = stoi(tmp);
            else if (num == 3)
                day = stoi(tmp);
            tmp = "";
            continue;
        }
        tmp += date[i];
    }
    tmp = "";
    num = 0;
    for (int i=0;i<startDate.size();i++){
        if (startDate[i] == '/') {
            num++;
            if (num == 1)
                yearStart = stoi(tmp);
            else if (num == 2)
                monthStart = stoi(tmp);
            else if (num == 3)
                dayStart = stoi(tmp);
            tmp = "";
            continue;
        }
        tmp += startDate[i];
    }
    return yearStart < yearDate || (yearStart == yearDate && monthStart < monthDate) || 
        (yearStart == yearDate && monthStart == monthDate && dayStart < day);
}

bool dateIsBeforeEnd(string date,string endDate){
    int yearDate,yearEnd,monthDate,monthEnd,day,dayEnd;
    string tmp = "";
    int num = 0;
    for (int i=0;i<date.size();i++){
        if (date[i] == '/') {
            num++;
            if (num == 1)
                yearDate = stoi(tmp);
            else if (num == 2)
                monthDate = stoi(tmp);
            else if (num == 3)
                day = stoi(tmp);
            tmp = "";
            continue;
        }
        tmp += date[i];
    }
    tmp = "";
    num = 0;
    for (int i=0;i<endDate.size();i++){
        if (endDate[i] == '/') {
            num++;
            if (num == 1)
                yearEnd = stoi(tmp);
            else if (num == 2)
                monthEnd = stoi(tmp);
            else if (num == 3)
                dayEnd = stoi(tmp);
            tmp = "";
            continue;
        }
        tmp += endDate[i];
    }
    return yearEnd > yearDate || (yearEnd == yearDate && monthEnd > monthDate) || 
        (yearEnd == yearDate && monthEnd == monthDate && dayEnd > day);
}

int extractCsvAndFindValids(fstream &file,string line,vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int eachCmd){
    int finalPrice;
    if (maxmins[eachCmd] == "MAX")
        finalPrice = 0;
    else
        finalPrice = numeric_limits<int> ::max();

    while(getline(file,line,'\n')){
        if (line[0] == 'd') continue;
        string tmp = "";
        int numOfColons = 0;
        bool isValid = false;
        for (int j=0;j<line.size();j++){ 
            if (line[j] == ',') {
                numOfColons++;
                if (numOfColons == 1) {
                    isValid = dateIsAfterStart(tmp,startDates[eachCmd]) && 
                        dateIsBeforeEnd(tmp,endDates[eachCmd]);
                }
                else if (numOfColons == 2) {
                    isValid = isValid && (stoi(tmp) == stoi(productIDs[eachCmd]));
                }
                tmp = "";
                continue;
            }
            tmp += line[j];
        }
        if (isValid) {
            if (maxmins[eachCmd] == "MAX") {
                if (stoi(tmp) > finalPrice)
                    finalPrice = stoi(tmp);
            }
            else
                if (stoi(tmp) < finalPrice)
                    finalPrice = stoi(tmp);
        }
    }
    if (finalPrice != 0 && finalPrice != 2147483647)
        return finalPrice;
    else
        return -1; 
}

bool processIfThisIsCsvChar(char name[256],string path,vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int eachCmd,fstream &resultFile) {
    string strName = "";
    for (int i=0;i<256;i++) {
        strName += name[i];
        if (fileIsCsvChar(name,i)) {
            strName += name[i+1];
            strName += name[i+2];
            strName += name[i+3];
            int pid = fork();

            if (processIsParent(pid))
                wait(NULL);

            else if (processIsChild(pid)) {
                fstream file;
                file.open(path + "/" + strName);
                string line;
                int result = extractCsvAndFindValids
                 (file,line,maxmins,productIDs,startDates,endDates,eachCmd);
                resultFile << result << endl;
                exit(0);
            }
            return true;
        }
    }
    return false;
}

bool processIsChild(int pid){
    return pid == 0;
}

void parentWriteRead(vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int fd[2]) {
    for (int i=0;i<maxmins.size();i++){
        char tmp[256] = "";
        char tmpOne[256] = "";
        char tmptwo[256] = "";
        char tmpthree[256] = "";
        for (int j=0;j<maxmins[i].size();j++)
            tmp[j] = maxmins[i][j];
        for (int j=0;j<productIDs[i].size();j++)
            tmpOne[j] = productIDs[i][j];
        for (int j=0;j<startDates[i].size();j++)
            tmptwo[j] = startDates[i][j];
        for (int j=0;j<endDates[i].size();j++)
            tmpthree[j] = endDates[i][j];

        write(fd[1],tmp,256);
        write(fd[1],tmpOne,256);
        write(fd[1],tmptwo,256);
        write(fd[1],tmpthree,256);   
    }
    wait(NULL);
    char tmp[256] = "";
    read(fd[0],tmp,256);
    // exit(0);
}

void childReadWrite(vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int fd[2],char (&result)[256],char (&resultone)[256],
  char (&resulttwo)[256],char (&resultthree)[256]){
    for (int i=0;i<maxmins.size();i++) {
        read(fd[0],result,256);
        read(fd[0],resultone,256);
        read(fd[0],resulttwo,256);
        read(fd[0],resultthree,256);
    }
    char myResult[256] = "";
    write(fd[1],myResult,256);
    // wait(NULL);
}

void readDir(string path,vector<string> maxmins,vector<string> productIDs,
 vector<string> startDates,vector<string> endDates,int whichLevel,int eachCmd,fstream& file) {

    DIR* parentDir = opendir(path.c_str());
    if (dirDoesntExist(parentDir))
        return;
        
    int fd[2];
    pipe(fd);
    mkfifo("myresult",0666);
    int pid = fork();

    char maxMins[256] = "";
    char prIDs[256] = "";
    char startings[256] = "";
    char endings[256] = "";

    if (processIsParent(pid)) {
        parentWriteRead(maxmins,productIDs,startDates,endDates,fd);
    }
    else if (processIsChild(pid)){
        childReadWrite(maxmins,productIDs,startDates,endDates,fd,maxMins,prIDs,startings,endings);

        struct dirent* dp;

        while ((dp = readdir(parentDir)) != NULL) {
            if (dp->d_name[0] == '.') continue;

            bool itsCsv = processIfThisIsCsvChar
            (dp->d_name,path,maxmins,productIDs,startDates,endDates,eachCmd,file);
            
            readDir(path + "/" + dp->d_name,maxmins,productIDs,startDates,
             endDates,whichLevel + 1,eachCmd,file);
        }
        exit(0);
    }
}

void showFinalResult(fstream& file,vector<string> maxmins,int eachCmd){
    string line;
    vector<string> lines;
    file.close();
    file.open("result.txt");
    while (getline(file,line,'\n'))
        lines.push_back(line);

    int result = stoi(lines[0]);
    if (maxmins[eachCmd] == "MAX"){
        for (int i=1;i < lines.size();i++) {
            if (stoi(lines[i]) == -1) continue;
            if (stoi(lines[i]) > result)
                result = stoi(lines[i]);
        }
    }
    else if (maxmins[eachCmd] == "MIN"){
        for (int i=1;i<lines.size();i++) {
            if (stoi(lines[i]) == -1) continue; 
            if (stoi(lines[i]) < result)
                result = stoi(lines[i]);
        }
    }
    cout << result << endl;
}