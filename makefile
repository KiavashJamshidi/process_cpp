CC = g++
FLAGS = -std=c++11
COMPILE_FLAGS = $(FLAGS) -c

StoreCalculator.out: mainGetCmd.o getcmd.o
	$(CC) mainGetCmd.o getcmd.o $(FLAGS) -o StoreCalculator.out

mainGetCmd.o: mainGetCmd.cpp getcmd.h
	$(CC) $(COMPILE_FLAGS) mainGetCmd.cpp

getcmd.o: getcmd.cpp getcmd.h
	$(CC) $(COMPILE_FLAGS) getcmd.cpp

.PHONY: clean

clean:
	rm *.o
	rm *.out