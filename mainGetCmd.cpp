#include <iostream>
#include <string>
#include <vector>
#include <dirent.h>
#include <filesystem>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fstream>
#include "getcmd.h"

using namespace std;

int main(int argc,char** argv) {
    vector<string> cmds;
    vector<string> maxmins,productIDs,startDates,endDates;
    int whichLevel = 1;
    getCmd(argv[1],cmds);
    splitCmds(cmds,maxmins,productIDs,startDates,endDates);
    for (int eachCmd=0;eachCmd<cmds.size();eachCmd++){
        fstream file;
        char fileName[256] = "result.txt";
        file.open(fileName,fstream::out);
        readDir(argv[2],maxmins,productIDs,startDates,endDates,whichLevel,eachCmd,file);
        showFinalResult(file,maxmins,eachCmd);
        file.close();
        remove(fileName);
    }
}